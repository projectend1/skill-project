import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Mainskillmapping from '@/types/Mainskillmapping'
import mainskillmappingService from '@/services/mainskillmapping'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useMainskillmappingStore = defineStore('mainskillmapping', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const editedMainskillmapping = ref<Mainskillmapping>({
    PLO_id: 0,
    namemainEng: '',
    level: 0,
    course: ''
  })
  const mainskillmappings = ref<Mainskillmapping[]>([])
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedMainskillmapping.value = {
        PLO_id: 0,
        namemainEng: '',
        level: 0,
        course: ''
      }
    }
  })

  async function getMainskillmappings() {
    loadingStore.isLoading = true
    try {
      const res = await mainskillmappingService.getMainskillmappings()
      mainskillmappings.value = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล Main Skill mapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveMainskillmapping() {
    loadingStore.isLoading = true
    try {
      if (editedMainskillmapping.value.id) {
        const res = await mainskillmappingService.updateMainskillmapping(
          editedMainskillmapping.value.id,
          editedMainskillmapping.value
        )
      } else {
        const res = await mainskillmappingService.saveMainskillmapping(editedMainskillmapping.value)
      }

      dialog.value = false
      await getMainskillmappings()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Main skill mapping ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteMainskillmapping(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await mainskillmappingService.deleteMainskillmapping(id)
      await getMainskillmappings()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ Main skill mapping ได้')
    }
    loadingStore.isLoading = false
  }

  function editMainskillmapping(mainskillmapping: Mainskillmapping) {
    editedMainskillmapping.value = JSON.parse(JSON.stringify(mainskillmapping))
    dialog.value = true
  }

  return {
    mainskillmappings,
    deleteMainskillmapping,
    dialog,
    editedMainskillmapping,
    saveMainskillmapping,
    editMainskillmapping,
    isTable,
    getMainskillmappings
  }
})
