import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Generalskillmapping from '@/types/Generalskillmapping'
import generalskillmappingService from '@/services/generalskillmapping'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useGeneralskillmappingStore = defineStore('generalskillmapping', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const editedGeneralskillmapping = ref<Generalskillmapping>({
    PLO_id: 0,
    course: '',
    namegeneral: '',
    expectLevel: 0
  })
  const generalskillmappings = ref<Generalskillmapping[]>([])

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedGeneralskillmapping.value = {
        PLO_id: 0,
        course: '',
        namegeneral: '',
        expectLevel: 0
      }
    }
  })
  async function getGeneralskillmappings() {
    loadingStore.isLoading = true
    try {
      const res = await generalskillmappingService.getGeneralskillmappings()
      generalskillmappings.value = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล General skill mapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveGeneralskillmapping() {
    loadingStore.isLoading = true
    try {
      if (editedGeneralskillmapping.value.id) {
        const res = await generalskillmappingService.updateGeneralskillmapping(
          editedGeneralskillmapping.value.id,
          editedGeneralskillmapping.value
        )
      } else {
        // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await generalskillmappingService.saveGeneralskillmapping(
          editedGeneralskillmapping.value
        )
      }

      dialog.value = false
      await getGeneralskillmappings()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก General skill mapping ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteGeneralskillmapping(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await generalskillmappingService.deleteGeneralskillmapping(id)
      await getGeneralskillmappings()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ General skill mapping ได้')
    }
    loadingStore.isLoading = false
  }

  function editGeneralskillmapping(generalskillmapping: Generalskillmapping) {
    editedGeneralskillmapping.value = JSON.parse(JSON.stringify(generalskillmapping))
    dialog.value = true
  }

  return {
    generalskillmappings,
    deleteGeneralskillmapping,
    dialog,
    editedGeneralskillmapping,
    // clear,
    saveGeneralskillmapping,
    editGeneralskillmapping,
    isTable,
    getGeneralskillmappings
  }
})
