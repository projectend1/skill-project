import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Profile from '@/types/Profile'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import profileService from '@/services/profile'
export const usevProfileStore = defineStore('profile', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const editedProfile = ref<Profile>({
    nameEng: '',
    nameTh: '',
    birthdate: '',
    gender: '',
    nationality: '',
    userType: '',
    id: 0,
    year: 0,
    facuityEng: '',
    email: '',
    email1: '',
    address: '',
    phone: 0,
    image: null
  })

  const profiles = ref<Profile[]>([])

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedProfile.value = {
        nameEng: '',
        nameTh: '',
        birthdate: '',
        gender: '',
        nationality: '',
        userType: '',
        id: 0,
        year: 0,
        facuityEng: '',
        email: '',
        email1: '',
        address: '',
        phone: 0,
        image: null
      }
    }
  })
  async function getProfiles() {
    loadingStore.isLoading = true
    try {
      const res = await profileService.getProfiles()
      profiles.value = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล Profile ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveProfile() {
    loadingStore.isLoading = true
    try {
      if (editedProfile.value.id) {
        const res = await profileService.updateProfile(editedProfile.value.id, editedProfile.value)
      } else {
        const res = await profileService.saveProfile(editedProfile.value)
      }

      dialog.value = false
      await getProfiles()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Profile ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteProfile(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await profileService.deleteProfile(id)
      await getProfiles()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ Profile ได้')
    }
    loadingStore.isLoading = false
  }

  function editProfile(profile: Profile) {
    editedProfile.value = JSON.parse(JSON.stringify(profile))
    dialog.value = true
  }

  return {
    profiles,
    deleteProfile,
    dialog,
    editedProfile,
    // clear,
    saveProfile,
    editProfile,
    isTable,
    getProfiles
  }
})
