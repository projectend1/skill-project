import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Mapping from '@/types/Mapping'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import mappingService from '@/services/mapping'  

export const useMappingStore = defineStore('Mapping', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const mappings = ref<Mapping[]>([])
  const editedMapping = ref<Mapping>({
    jobId: 0,
    skillId: 0
  })

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedMapping.value = {
        jobId: 0,
    skillId: 0
      }
    }
  })
  async function getMappings() {
    loadingStore.isLoading = true
    try {
      const res = await mappingService.getMappings()
      console.log('Mapping data:', res.data);
      mappings.value = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Mapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveMapping() {
    loadingStore.isLoading = true
    try {
      

      if (editedMapping.value.id) {
        const res = await mappingService.updateMapping(editedMapping.value.id, editedMapping.value);
      } else {
        const res = await mappingService.saveMapping(editedMapping.value);
      }

      dialog.value = false;
      await getMappings();
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Mapping ได้');
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  async function deleteMapping(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await mappingService.deleteMapping(id)
      await getMappings()
    } catch (e) {
      messageStore.showError('ไม่สามารถลบ Mapping ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  function editMapping(mapping: Mapping) {
    editedMapping.value = JSON.parse(JSON.stringify(mapping))
    dialog.value = true
  }

  return {
    mappings,
    deleteMapping,
    dialog,
    editedMapping,
    getMappings,
    saveMapping,
    editMapping,
    isTable,
    // jobId: mappings.value.map((mapping) => mapping.jobId),
  }
})
