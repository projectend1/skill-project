import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Addstudent from '@/types/Addstudent'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import addstudentService from '@/services/addstudent'

export const useAddstudentStore = defineStore('Addstudent', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  // const getSubject = ref<Subject[]>([]);
  const dialog = ref(false)
  const isTable = ref(true)
  const addstudents = ref<Addstudent[]>([])
  const yearlist = ref<String[]>([])
  const editedAddstudent = ref<Addstudent>({
    idstudent: '',
    name: '',
    lastname: '',
    year: '',
    mainLevel: '',
    generalLevel: ''
  })

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedAddstudent.value = {
        idstudent: '',
        name: '',
        lastname: '',
        year: '',
        mainLevel: '',
        generalLevel: ''
      }
    }
  })
  async function getAddstudents() {
    loadingStore.isLoading = true
    try {
      const res = await addstudentService.getAddstudents()
      addstudents.value = res.data
      getYear()
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Addstudent ได้')
    }
    loadingStore.isLoading = false
  }

  async function getAddstudentByYear(id: string) {
    loadingStore.isLoading = true
    try {
      const res = await addstudentService.getAddstudentByYear(id)
      addstudents.value = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Addstudent ได้')
    }
    loadingStore.isLoading = false
  }

  function getYear() {
    const yearSet = new Set(addstudents.value.map((x) => x.year))
    const yearSort = Array.from(yearSet).sort()
    yearlist.value = yearSort
  }

  async function saveAddstudent() {
    loadingStore.isLoading = true
    try {
      if (editedAddstudent.value.id) {
        const res = await addstudentService.updateAddstudent(
          editedAddstudent.value.id,
          editedAddstudent.value
        )
      } else {
        const res = await addstudentService.saveAddstudent(editedAddstudent.value)
      }

      dialog.value = false
      await getAddstudents()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Addstudent ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteAddstudent(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await addstudentService.deleteAddstudent(id)
      await getAddstudents()
    } catch (e) {
      messageStore.showError('ไม่สามารถลบ Addstudent ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  function editAddstudent(addstudent: Addstudent) {
    editedAddstudent.value = JSON.parse(JSON.stringify(addstudent))
    dialog.value = true
  }

  // const clear = () => {
  //   editedSubject.value = {
  //     id: -1,
  //     course: '',
  //     nameEng: '',
  //     nameTh: '',
  //     descriptionEng: '',
  //     descriptionTh: ''
  //   }
  // }

  return {
    addstudents,
    deleteAddstudent,
    dialog,
    editedAddstudent,
    getAddstudents,
    // clear,
    saveAddstudent,
    editAddstudent,
    isTable,
    getAddstudentByYear,
    yearlist
  }
})
