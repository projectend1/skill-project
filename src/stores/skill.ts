import { ref, watch } from 'vue';
import { defineStore } from 'pinia';
import type Skill from '@/types/Skill';
import skillService from '@/services/skill';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const useSkillStore = defineStore('skill', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const editedSkill = ref<Skill>({
    nameEng: '',
    nameTh: '',
    descriptionEng: '',
    descriptionTh: '',
    mainSkill: '',
    
  });
  const skills = ref<Skill[]>([]);
  
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedSkill.value = {
    nameEng: '',
    nameTh: '',
    descriptionEng: '',
    descriptionTh: '',
    mainSkill: '',
    
      };
    }
  });
  async function getSkills() {
    loadingStore.isLoading = true;
    try {
      const res = await skillService.getSkills();
      skills.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Skill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveSkill() {
    loadingStore.isLoading = true;
    try {
      if (editedSkill.value.id) {
        const res = await skillService.updateSkill(
          editedSkill.value.id,
          editedSkill.value
        );
      } else {
         // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await skillService.saveSkill(editedSkill.value);
      }

      dialog.value = false;
      await getSkills();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Skill ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  

  async function deleteSkill(id: number){
    loadingStore.isLoading = true;
    try {
      const res = await skillService.deleteSkill(id);
      await getSkills();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Skill ได้");
    }
    loadingStore.isLoading = false;

  }

  
  function editSkill(skill: Skill)  {
    editedSkill.value = JSON.parse(JSON.stringify(skill));
    dialog.value = true;
  }

 
  return {
    skills,
    deleteSkill,
    dialog,
    editedSkill,
    // clear,
    saveSkill,
    editSkill,
    isTable,
    getSkills
  }
})
