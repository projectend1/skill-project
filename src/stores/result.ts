import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Result from '@/types/Result'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import resultService from '@/services/result'
import addskillService from '@/services/addskill'

export const useResultStore = defineStore('Result', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  // const getSubject = ref<Subject[]>([]);
  const dialog = ref(false)
  const isTable = ref(true)
  const results = ref<Result[]>([])
  const editedResult = ref<Result>({
    course: 0,
    namemainEng: 0,
    namegeneral: 0,
    alllevel: 0,
    idstudent:0,
    genlevel:''
  })

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedResult.value = {
        course: 0,
        namemainEng: 0,
        namegeneral: 0,
        alllevel: 0,
        idstudent:0,
        genlevel:''
      }
    }
  })
  
  async function getResults() {
    loadingStore.isLoading = true
    try {
      const res = await resultService.getResults()
      results.value = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Result ได้')
    }
    loadingStore.isLoading = false
  }
  async function getResultByStudent(id:number) {
    loadingStore.isLoading = true
    try {
      const res = await resultService.getResultByStudent(id)
      results.value = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Result ได้')
    }
    loadingStore.isLoading = false
  }


  async function saveResult() {
    loadingStore.isLoading = true
    try {
      if (editedResult.value.id) {
        const res = await resultService.updateResult(editedResult.value.id, editedResult.value)
      } else {
        const res = await resultService.saveResult(editedResult.value)
      }

      dialog.value = false
      await getResults()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Result ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteResult(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await addskillService.deleteAddskill(id)
      await getResults()
    } catch (e) {
      messageStore.showError('ไม่สามารถลบ Result ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  function editResult(result: Result) {
    editedResult.value = JSON.parse(JSON.stringify(result))
    dialog.value = true
  }



  return {
    results,
    deleteResult,
    dialog,
    editedResult,
    getResults,
    // clear,
    saveResult,
    editResult,
    isTable,
    getResultByStudent
  }
})
