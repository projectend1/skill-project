import { ref } from "vue";
import { defineStore } from "pinia";
import type Information from "@/types/Information";

export const useInformationStore = defineStore("informationt", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const editedInformation = ref<Information>({ id: -1, course: -1,nameEng: "", nameTh: "" , descriptionEng:"", descriptionTh:""});
  let lastId = 4;
  const informations = ref<Information[]>([
    { id: 1,course:89022367, nameEng: "Object-Oriented Programming and Data Structures", 
    nameTh: "การโปรแกรมเชิงวัตถุและโครงสร้างข้อมูล" ,
    descriptionEng:"Principles of data structure; arrays, linked lists, stacks, queues; sorting algorithms; hashing and hash-based data structures; object-oriented programming concept; polymorphism; encapsulation; inheritance; design principles and software architecture; unit testing and test-driven development", 
    descriptionTh:"หลักการของโครงสร้างข้อมูล อาร์เรย์ ลิงค์ลิส สแต็ค คิว อัลกอริทึมการเรียงลำดับการแฮชและโครงสร้างข้อมูลแบบแฮช แนวคิดการเขียนโปรแกรมเชิงวัตถุการมีได้หลายรูปแบบ การห่อหุ้ม การสืบทอด หลักการออกแบบและสถาปัตยกรรมซอฟต์แวร์ วิธีการทดสอบการทำงานของซอฟแวร์ในแต่ละหน่วยและกระบวนการในการพัฒนาซอฟต์แวร์ที่ใช้การทดสอบเป็นตัวขับเคลื่อน" },
    { id: 2,course:89022467, nameEng: "Unix Tools and System Administration", nameTh: "เครื่องมือและการดูแลระบบปฏิบัติการยูนิกซ์ " ,
    descriptionEng:"Introduction to Unix operating system; navigation and essential commands regarding file systems in Unix; file management and text processing utilities; system resources and process management; user and group management; permissionsand access controls; system performance monitoring and optimization; shell scripting for automation and task scheduling; configuration management tools; security logging and auditing; backup and recovery; emerging trends in Unix system administration", 
    descriptionTh:"พื้นฐานเกี่ยวกับระบบปฏิบัติการยูนิกซ์ การนำทางและคำสั่งที่จำเป็นเกี่ยวกับระบบไฟล์ในยูนิกซ์ เครื่องมือจัดการไฟล์และประมวลผลข้อความ การจัดการทรัพยากรระบบและการจัดการกระบวนการ การจัดการผู้ใช้และกลุ่มผู้ใช้ การอนุญาตและการควบคุมการเข้าถึง การตรวจสอบประสิทธิภาพของระบบและการเพิ่มประสิทธิภาพ การเขียนเชลล์สคริปต์สำหรับการทำงานอัตโนมัติและการกำหนดตารางงาน เครื่องมือการจัดการการกำหนดค่า การบันทึกความปลอดภัยและการตรวจสอบ การสำรองและการกู้คืน แนวโน้มที่เกิดขึ้นใหม่ในการบริหารจัดการระบบยูนิกซ"},
    { id: 3,course:89022567 , nameEng: "Basic Electronics and Internet of Things", nameTh: "พื้นฐานอิเล็กทรอนิกส์และอินเทอร์เน็ตของทุกสรรพสิ่ง " ,
    descriptionEng:"Basic electronics; internet of things concept; IoT technology; microcontroller architecture; input/output peripheral devices; connecting input-output and display devices; IoT hardware device; interaction between IoT’s software and hardware; operating systems for IoT devices; cloud services for IoT; data analytics for IoT systems; IoT applications: smart homes, smart cities, healthcare, and industrial automation; security and privacy related to IoT", 
    descriptionTh:"ความรู้เบื้องต้นเกี่ยวกับอิเล็กทรอนิกส์ แนวคิดของอินเตอร์เน็ตในทุกสิ่ง เทคโนโลยีด้านไอโอที สถาปัตยกรรมไมโครคอนโทรลเลอร์ อุปกรณ์อินพุต/เอาท์พุตและอุปกรณ์ต่อพ่วง การเชื่อมต่ออินพุท-เอาท์พุทและอุปกรณ์แสดงผล อุปกรณ์ฮาร์ดแวร์ไอโอที การโต้ตอบระหว่างซอฟต์แวร์และฮาร์ดแวร์ในอุปกรณ์ไอโอที ระบบปฏิบัติการสำหรับอุปกรณ์ไอโอที บริการคลาวด์สำหรับอินเทอร์เน็ตของสรรพสิ่ง การวิเคราะห์ข้อมูลสำหรับระบบ IoT แอปพลิเคชัน IoT เช่น บ้านอัจฉริยะ เมืองอัจฉริยะ การดูแลสุขภาพ และระบบอัตโนมัติทางอุตสาหกรรม ความปลอดภัยและความเป็นส่วนตัวที่เกี่ยวข้องกับ IoT"},
  ]);

  const deleteInformation = (id: number): void => {
    const index = informations.value.findIndex((item) => item.id === id);
    informations.value.splice(index, 1);
  };

  const saveInformation = () => {
    if (editedInformation.value.id < 0) {
      editedInformation.value.id = lastId++;
      informations.value.push(editedInformation.value);
    } else {
      const index = informations.value.findIndex(
        (item) => item.id === editedInformation.value.id
      );
      informations.value[index] = editedInformation.value;
    }
    dialog.value = false;
    clear();
  };

  const editInformation= (information: Information) => {
    editedInformation.value = { ...information};
    dialog.value = true;
  };

  const clear = () => {
    editedInformation.value = { id: -1,course:"", nameEng: "", nameTh: "" ,descriptionEng:"", descriptionTh:""};
  };
 
  return {
    informations,
    deleteInformation,
    dialog,
    editedInformation,
    clear,
    saveInformation,
    editInformation,
    isTable,
    
   
  
  };
});
