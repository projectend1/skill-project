import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Generalskill from '@/types/Generalskill'
import generalskillService from '@/services/generalskill'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useGeneralskillStore = defineStore('generalskill', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const editedGeneralskill = ref<Generalskill>({
    namegeneral:'',
    generaldescription: ''
  })
  const generalskills = ref<Generalskill[]>([])

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedGeneralskill.value = {
        namegeneral:'',
        generaldescription: ''
      }
    }
  })
  async function getGeneralskills() {
    loadingStore.isLoading = true
    try {
      const res = await generalskillService.getGeneralskills()
      generalskills.value = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล General skill ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveGeneralskill() {
    loadingStore.isLoading = true
    try {
      if (editedGeneralskill.value.id) {
        const res = await generalskillService.updateGeneralskill(
          editedGeneralskill.value.id,
          editedGeneralskill.value
        )
      } else {
        // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await generalskillService.saveGeneralskill(editedGeneralskill.value)
      }

      dialog.value = false
      await getGeneralskills()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก General skill ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteGeneralskill(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await generalskillService.deleteGeneralskill(id)
      await getGeneralskills()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ General skill ได้')
    }
    loadingStore.isLoading = false
  }

  function editGeneralskill(generalskill: Generalskill) {
    editedGeneralskill.value = JSON.parse(JSON.stringify(generalskill))
    dialog.value = true
  }

  return {
    generalskills,
    deleteGeneralskill,
    dialog,
    editedGeneralskill,
    // clear,
    saveGeneralskill,
    editGeneralskill,
    isTable,
    getGeneralskills
  }
})
