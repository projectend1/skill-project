import { ref } from "vue";
import { defineStore } from "pinia";
import type Information2 from "@/types/Information2";

export const useInformation2Store = defineStore("informationt2", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const editedInformation2 = ref<Information2>({ id: -1, idStudent:" ",name:" ",skill: "" , level:""});
  let lastId = 4;
  const informations2 = ref<Information2[]>([
    { id: 1,idStudent:"67160007" , name:"Kim Lee" ,skill: "AI Domain Expertise", level:"5"},
    { id: 1,idStudent:"67160007" , name:"Kim Lee" ,skill: "AI Domain Expertise", level:"5"},
    { id: 1,idStudent:"67160007" , name:"Kim Lee" ,skill: "AI Domain Expertise", level:"5"},
  ]);

  const deleteInformation2 = (id: number): void => {
    const index = informations2.value.findIndex((item) => item.id === id);
    informations2.value.splice(index, 1);
  };

  const saveInformation2 = () => {
    if (editedInformation2.value.id < 0) {
      editedInformation2.value.id = lastId++;
      informations2.value.push(editedInformation2.value);
    } else {
      const index = informations2.value.findIndex(
        (item) => item.id === editedInformation2.value.id
      );
      informations2.value[index] = editedInformation2.value;
    }
    dialog.value = false;
    clear();
  };

  const editInformation2= (information2: Information2) => {
    editedInformation2.value = { ...information2};
    dialog.value = true;
  };

  const clear = () => {
    editedInformation2.value = { id: -1,idStudent:" ",name:" ",skill: "" , level:""};
  };
 
  return {
    informations2,
    deleteInformation2,
    dialog,
    editedInformation2,
    clear,
    saveInformation2,
    editInformation2,
    isTable,
    
   
  
  };
});
