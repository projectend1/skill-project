import { ref, watch } from 'vue';
import { defineStore } from 'pinia';
import type Mainskill from '@/types/Mainskill';
import mainskillService from '@/services/mainskill';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const useMainskillStore = defineStore('mainskill', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const editedMainskill = ref<Mainskill>({
    namemainEng: '',
    namemainTh: '',
    descriptionEng: '',
    descriptionTh: '',
   
    
  });
  const mainskills = ref<Mainskill[]>([]);
  
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMainskill.value = {
    namemainEng: '',
    namemainTh: '',
    descriptionEng: '',
    descriptionTh: '',
   
    
      };
    }
  });
  async function getMainskills() {
    loadingStore.isLoading = true;
    try {
      const res = await mainskillService.getMainskills();
      mainskills.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Mainskillได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveMainskill() {
    loadingStore.isLoading = true;
    try {
      if (editedMainskill.value.id) {
        const res = await mainskillService.updateMainskill(
          editedMainskill.value.id,
          editedMainskill.value
        );
      } else {
         // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await mainskillService.saveMainskill(editedMainskill.value);
      }

      dialog.value = false;
      await getMainskills();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Mainskill ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  

  async function deleteMainskill(id: number){
    loadingStore.isLoading = true;
    try {
      const res = await mainskillService.deleteMainskill(id);
      await getMainskills();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Mainskill ได้");
    }
    loadingStore.isLoading = false;

  }

  
  function editMainskill(mainskill: Mainskill)  {
    editedMainskill.value = JSON.parse(JSON.stringify(mainskill));
    dialog.value = true;
  }

  // const clear = () => {
  //   editedSkill.value = {
  //     id: -1,
  //     nameEng: '',
  //     nameTh: '',
  //     descriptionEng: '',
  //     descriptionTh: '',
  //     mainSkill: '',
  //     generalSkill: ''
  //   }
  // }
  return {
    mainskills,
    deleteMainskill,
    dialog,
    editedMainskill,
    // clear,
    saveMainskill,
    editMainskill,
    isTable,
    getMainskills
  }
})
