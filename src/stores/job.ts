import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Job from '@/types/Job'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import jobService from '@/services/job'
export const useJobStore = defineStore('job', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const editedJob = ref<Job>({
    nameEng: '',
    nameTh: '',
    descriptionEng: '',
    descriptionTh: ''
  })

  const jobs = ref<Job[]>([])

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedJob.value = {
        nameEng: '',
        nameTh: '',
        descriptionEng: '',
        descriptionTh: ''
      }
    }
  })
  async function getJobs() {
    loadingStore.isLoading = true
    try {
      const res = await jobService.getJobs()
      jobs.value = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล Job ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveJob() {
    loadingStore.isLoading = true
    try {
      if (editedJob.value.id) {
        const res = await jobService.updateJob(editedJob.value.id, editedJob.value)
      } else {
        const res = await jobService.saveJob(editedJob.value)
      }

      dialog.value = false
      await getJobs()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Job ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteJob(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await jobService.deleteJob(id)
      await getJobs()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ Job ได้')
    }
    loadingStore.isLoading = false
  }

  function editJob(job: Job) {
    editedJob.value = JSON.parse(JSON.stringify(job))
    dialog.value = true
  }


  return {
    jobs,
    deleteJob,
    dialog,
    editedJob,
    // clear,
    saveJob,
    editJob,
    isTable,
    getJobs
  }
})
