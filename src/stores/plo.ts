import { ref, watch } from 'vue';
import { defineStore } from 'pinia';
import type PLO from '@/types/PLO';
import ploService from '@/services/plo';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const usePLOStore = defineStore('plo', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const editedPLO = ref<PLO>({
    PLO_id:'',
    PLO_aptitude:'', 
    PLO_result: ''
    
  });
  const plos = ref<PLO[]>([]);
  
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedPLO.value = {
        PLO_id:'',
        PLO_aptitude:'', 
        PLO_result: ''
    
      };
    }
  });
  async function getPLOs() {
    loadingStore.isLoading = true;
    try {
      const res = await ploService.getPLOs();
      plos.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล PLO ได้");
    }
    loadingStore.isLoading = false;
  }

  async function savePLO() {
    loadingStore.isLoading = true;
    try {
      if (editedPLO.value.id) {
        const res = await ploService.updatePLO(
          editedPLO.value.id,
          editedPLO.value
        );
      } else {
         // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await ploService.savePLO(editedPLO.value);
      }

      dialog.value = false;
      await getPLOs();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก PLO ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  

  async function deletePLO(id: number){
    loadingStore.isLoading = true;
    try {
      const res = await ploService.deletePLO(id);
      await getPLOs();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ PLO ได้");
    }
    loadingStore.isLoading = false;

  }

  
  function editPLO(plo: PLO)  {
    editedPLO.value = JSON.parse(JSON.stringify(plo));
    dialog.value = true;
  }

  // const clear = () => {
  //   editedSkill.value = {
  //     id: -1,
  //     nameEng: '',
  //     nameTh: '',
  //     descriptionEng: '',
  //     descriptionTh: '',
  //     mainSkill: '',
  //     generalSkill: ''
  //   }
  // }
  return {
    plos,
    deletePLO,
    dialog,
    editedPLO,
    // clear,
    savePLO,
    editPLO,
    isTable,
    getPLOs
  }
})
