import { computed, reactive, ref, toRefs, watch } from 'vue'
import { defineStore } from 'pinia'
import type Addskill from '@/types/Addskill'
import addskillService from '@/services/addskill'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import type Subject from '../types/Subject'
import subjectService from '@/services/subject'
import mainskillmappingService from '@/services/mainskillmapping'
import type Mainskillmapping from '@/types/Mainskillmapping'
import type Generalskillmapping from '@/types/Generalskillmapping'
import generalskillmappingService from '@/services/generalskillmapping'
import type Addstudent from '../types/Addstudent'
import addstudentService from '@/services/addstudent'
import mainskillService from '@/services/mainskill'
import generalskillService from '@/services/generalskill'
import type Mainskill from '@/types/Mainskill'
import type Generalskill from '@/types/Generalskill'
import mainskill from '@/services/mainskill';
import generalskill from '@/services/generalskill';

export const useAddskillStore = defineStore('addskill', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const dialog = ref(false)
  const isTable = ref(true)
  const yearlist = ref<String[]>([])
  // Your store functions here...
  interface State {
    editedAddskill: Addskill
    addskills: Addskill[]
    subjects: Subject[]
    mainskillmappings: Mainskillmapping[]
    generalskillmappings: Generalskillmapping[]
    students: Addstudent[]
    mainskill:Mainskill[]
    generalskill:Generalskill[]
  }

  const state: State = reactive({
    editedAddskill: {
      subjectId: 0,
      mainskillmapping: 0,
      generalskillmapping: 0,
      year: '',
      studentId: '',
      alllevel: 0,
      mainskill:'',
      generalskill:'',
      genlevel:''
    },
    addskills: [],
    subjects: [],
    mainskillmappings: [],
    generalskillmappings: [],
    students: [],
    mainskill:[],
    generalskill:[]
  })

  const subjects = computed(() => state.subjects)
  const mainskillmappings = computed(() => state.mainskillmappings)
  const generalskillmappings = computed(() => state.generalskillmappings)
  const students = computed(() => state.students)
  const mainskill = computed(() => state.mainskill)
  const generalskill = computed(() => state.generalskill)

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      state.editedAddskill = {
        subjectId: 0,
        mainskillmapping: 0,
        generalskillmapping: 0,
        year: '',
        studentId: '',
        alllevel: 0,
        genlevel:''
      }
    }
  })

  async function getSubjects() {
    loadingStore.isLoading = true
    try {
      const res = await subjectService.getSubjects()
      state.subjects = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล subjects ได้')
    }
    loadingStore.isLoading = false
  }

  async function getMainskills() {
    loadingStore.isLoading = true
    try {
      const res = await mainskillService.getMainskills()
      state.mainskill = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล mainskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getMainskillmappings() {
    loadingStore.isLoading = true
    try {
      const res = await mainskillmappingService.getMainskillmappings()
      state.mainskillmappings = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล mainskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getMainskillmappingsBySubjectId(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await mainskillmappingService.getMainskillmappingsBySubjectId(id)
      state.mainskillmappings = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล mainskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getGeneralskills() {
    loadingStore.isLoading = true
    try {
      const res = await generalskillService.getGeneralskills()
      state.generalskill = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล generalskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getGeneralskillmappings() {
    loadingStore.isLoading = true
    try {
      const res = await generalskillmappingService.getGeneralskillmappings()
      state.generalskillmappings = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล generalskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getGeneralskillmappingsBySubjectId(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await generalskillmappingService.getGeneralskillmappingsBySubjectId(id)
      state.generalskillmappings = res.data
    } catch (e) {
      console.log(e)
      console.log(state.generalskillmappings)
      messageStore.showError('ไม่สามารถดึงข้อมูล generalskillmapping ได้')
    }
    loadingStore.isLoading = false
  }

  async function getAddstudents() {
    loadingStore.isLoading = true
    try {
      const res = await addstudentService.getAddstudents()
      state.students = res.data
      getYear()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล getAddstudents ได้')
    }
    loadingStore.isLoading = false
  }

  function getYear() {
    const yearSet = new Set(students.value.map((x) => x.year))
    const yearSort = Array.from(yearSet).sort()
    yearlist.value = yearSort
  }

  async function getAddstudentByYear(id: string) {
    loadingStore.isLoading = true
    try {
      const res = await addstudentService.getAddstudentByYear(id)
      state.students = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Addstudent ได้')
    }
    loadingStore.isLoading = false
  }
  // สร้างฟังก์ชัน getAddskills
  async function getAddskills() {
    loadingStore.isLoading = true
    try {
      const res = await addskillService.getAddskills()
      state.addskills = res.data
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถดึงข้อมูล add skill ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveAddskill(
    selectedSubject: string,
    selectedMainskillmapping: string,
    selectedGeneralskillmapping: string
  ) {
    loadingStore.isLoading = true
    try {
      const filterStudent = students.value.filter(
        (std) =>  'mainLevel' in std || 'generalLevel' in std
      )
     
      const filterSubject = subjects.value.find(
        (sbj) => sbj.course === selectedSubject.split(' ')[0]
      )

 
      console.log(mainskill.value)
      const filterMain = mainskill.value.find(
        (msm) => msm.namemainEng === selectedMainskillmapping
      )
   
      console.log(generalskill.value)
      const filterGeneral = generalskill.value.find(
        (gsm) => gsm.namegeneral === selectedGeneralskillmapping
      )
   
      // ตรวจสอบว่ามีข้อมูลที่เลือกหรือไม่
      if (!filterSubject) {
        throw new Error('กรุณาเลือกข้อมูลให้ครบถ้วน')
      }
    

      // ตรวจสอบว่าหากไม่มีข้อมูลที่เลือก ให้ใช้ค่าเริ่มต้นเป็น 0
      const subjectId = filterSubject.id || 0
      const mainskillmappingId = filterMain ? filterMain.id || 0 : 0
      const generalskillmappingId = filterGeneral ? filterGeneral.id || 0 : 0
      
      // console.log(filterMain?.id)
      // console.log(filterGeneral?.id)
      // console.log(generalskillmappings.value)
      const studentlist: Addskill[] = []
      filterStudent.forEach((std) => {
        let mainlevel = 0
        const genlevel  = std.generalLevel
        if (std.mainLevel) {
          switch (std.mainLevel) {
            case '1':
              mainlevel = 1
              break
            case '2':
              mainlevel = 2
              break
            case '3':
              mainlevel = 3
              break
            case '4':
              mainlevel = 4
              break
            case '5':
              mainlevel = 5
              break
          }
      
          
        }
        const temp: Addskill = {
          subjectId: subjectId,
          mainskillmapping: mainskillmappingId,
          generalskillmapping: generalskillmappingId,
          year: std.year,
          studentId: std.id!.toString() ,
          alllevel: mainlevel,
          genlevel: genlevel
        }
        studentlist.push(temp)
      })
      const res = await addskillService.saveAddskill(studentlist)
      console.log(studentlist)
      dialog.value = false
      await getAddskills()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก add skill ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  async function deleteAddskill(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await addskillService.deleteAddskill(id)
      await getAddskills()
    } catch (e) {
      console.log(e)
      messageStore.showError('ไม่สามารถลบ add skill ได้')
    }
    loadingStore.isLoading = false
  }

  function editAddskill(addskill: Addskill) {
    state.editedAddskill = JSON.parse(JSON.stringify(addskill))
    dialog.value = true
  }

  return {
    ...toRefs(state),
    // addskills,
    deleteAddskill,
    dialog,
    // clear,
    saveAddskill,
    editAddskill,
    isTable,
    getAddskills,
    getSubjects,
    subjects,
    getMainskillmappings,
    mainskillmappings,
    getGeneralskillmappings,
    generalskillmappings,
    getAddstudents,
    students,
    getMainskillmappingsBySubjectId,
    getGeneralskillmappingsBySubjectId,
    yearlist,
    getAddstudentByYear,
    getMainskills,
    getGeneralskills
  }
})
