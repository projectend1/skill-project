import { ref, watch } from 'vue';
import { defineStore } from 'pinia';
import type Certificates from '@/types/Certificates' 
import certificatesService from '@/services/certificates';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const useCertificatesStore = defineStore('certificates', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const editedCertificates = ref<Certificates>({
    image: null,
  description: '',
  link: null
    
  });
  const certificates = ref<Certificates[]>([]);
  // สร้าง object สำหรับ Certificates โดยกำหนดค่าเริ่มต้น
  const defaultCertificates: Certificates = {
    image: null,
    description: '',
    link: null
  };

  // สร้าง watch สำหรับตรวจสอบการเปลี่ยนแปลงใน dialog
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCertificates.value = {
        image: null,
  description: '',
  link: null
    
      };
      // เมื่อ dialog เป็น falsy (null, undefined, false)
      // กำหนดค่าเริ่มต้นใหม่ให้กับ editedCertificates
      editedCertificates.value = { ...defaultCertificates };
    }
  });
  async function getCertificates() {
    loadingStore.isLoading = true;
    try {
      const res = await certificatesService.getCertificates();
      certificates.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Skill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCertificates() {
    loadingStore.isLoading = true;
    try {
      if (editedCertificates.value.id) {
        const res = await certificatesService.updateCertificates(
          editedCertificates.value.id,
          editedCertificates.value
        );
      } else {
        // ในที่นี้, คุณต้องเขียนฟังก์ชันหรือใช้ service ที่เหมาะสม
        // เพื่อบันทึกข้อมูลทักษะลงในฐานข้อมูล
        const res = await certificatesService.saveCertificates(editedCertificates.value);
      }
  
      dialog.value = false;
      await getCertificates();
  
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Skill ได้");
      console.error(e);
    }
    loadingStore.isLoading = false;
  }
  

  async function deleteCertificates(id: number){
    loadingStore.isLoading = true;
    try {
      const res = await certificatesService.deleteCertificates(id);
      await getCertificates();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Skill ได้");
    }
    loadingStore.isLoading = false;

  }

  
  function editCertificates(certificates: Certificates)  {
    editedCertificates.value = JSON.parse(JSON.stringify(certificates));
    dialog.value = true;
  }

  // const clear = () => {
  //   editedSkill.value = {
  //     id: -1,
  //     nameEng: '',
  //     nameTh: '',
  //     descriptionEng: '',
  //     descriptionTh: '',
  //     mainSkill: '',
  //     generalSkill: ''
  //   }
  // }
  return {
    certificates,
    deleteCertificates,
    dialog,
    editedCertificates,
    // clear,
    saveCertificates,
    editCertificates,
    isTable,
    getCertificates
  }
})
