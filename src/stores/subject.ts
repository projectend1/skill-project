import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Subject from '@/types/Subject'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import subjectService from '@/services/subject'

export const useSubjectStore = defineStore('Subject', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  // const getSubject = ref<Subject[]>([]);
  const dialog = ref(false)
  const isTable = ref(true)
  const subjects = ref<Subject[]>([])
  const editedSubject = ref<Subject>({
    course: '',
    nameEng: '',
    nameTh: '',
    descriptionEng: '',
    descriptionTh: ''
  })

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedSubject.value = {
        course: '',
        nameEng: '',
        nameTh: '',
        descriptionEng: '',
        descriptionTh: ''
      }
    }
  })
  async function getSubjects() {
    loadingStore.isLoading = true
    try {
      const res = await subjectService.getSubjects()
      subjects.value = res.data
    } catch (e) {
      // console.log(e);
      messageStore.showError('ไม่สามารถดึงข้อมูล Subject ได้')
    }
    loadingStore.isLoading = false
  }

  async function saveSubject() {
    loadingStore.isLoading = true
    try {
      if (editedSubject.value.id) {
        const res = await subjectService.updateSubject(
          editedSubject.value.id,
          editedSubject.value
        );
      } else {
        const res = await subjectService.saveSubject(editedSubject.value);
      }

      dialog.value = false;
      await getSubjects()
    } catch (e) {
      messageStore.showError('ไม่สามารถบันทึก Subject ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  
  async function deleteSubject(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await subjectService.deleteSubject(id)
      await getSubjects()
    } catch (e) {
      messageStore.showError('ไม่สามารถลบ Subject ได้')
      console.log(e)
    }
    loadingStore.isLoading = false
  }

  function editSubject(subject: Subject) {
    editedSubject.value = JSON.parse(JSON.stringify(subject))
    dialog.value = true
  }

  return {
    subjects,
    deleteSubject,
    dialog,
    editedSubject,
    getSubjects,
    // clear,
    saveSubject,
    editSubject,
    isTable
  }
})
