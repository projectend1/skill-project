import type Skill from "@/types/Skill";
import http from "./axios";

function getSkills() {
  return http.get( `/skill`);
}
function getSkillOne(id: number) {
  return http.get(`/skill/${id}`);
}

function saveSkill(skill: Skill) {
  return http.post(`/skill`, skill);
}

function updateSkill(id: number, skill: Skill) {
  return http.patch(`/skill/${id}`, skill);
}

function deleteSkill(id: number) {
  return http.delete(`/skill/${id}`);
}

export default { getSkills, saveSkill, updateSkill, deleteSkill,getSkillOne };
