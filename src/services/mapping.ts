import type Mapping from "@/types/Mapping";
import http from "./axios";
function getMappings() {
  return http.get(`/mapping`);
}
function getMappingOne(id: number) {
  return http.get(`/mapping/${id}`);
}

function saveMapping(mapping: Mapping) {
  return http.post(`/mapping`, mapping);
}

function updateMapping(id: number, mapping: Mapping) {
  return http.patch(`/mapping/${id}`, mapping);
}

function deleteMapping(id: number) {
  return http.delete(`/mapping/${id}`);
}


export default { getMappings, saveMapping, updateMapping, deleteMapping ,getMappingOne};
