import type Generalskillmapping from "@/types/Generalskillmapping";
import http from "./axios";

function getGeneralskillmappings() {
  return http.get( `/generalskillmapping`);  
}
function getGeneralskillmappingsBySubjectId(id: number) {
  return http.get( `/generalskillmapping/getbysubjectId/${id}`);  
}
function getGeneralskillmappingOne(id: number) {
  return http.get(`/generalskillmapping/${id}`);
}

function saveGeneralskillmapping(generalskillmapping: Generalskillmapping) {
  return http.post(`/generalskillmapping`, generalskillmapping);
}

function updateGeneralskillmapping(id: number, generalskillmapping: Generalskillmapping) {
  return http.patch(`/generalskillmapping/${id}`, generalskillmapping);
}

function deleteGeneralskillmapping(id: number) {
  return http.delete(`/generalskillmapping/${id}`);
}

export default { getGeneralskillmappings, saveGeneralskillmapping, updateGeneralskillmapping, deleteGeneralskillmapping,getGeneralskillmappingOne,getGeneralskillmappingsBySubjectId };
