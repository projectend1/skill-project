import type Addskill from "@/types/Addskill";
import http from "./axios";

function getAddskills() {
    return http.get(`/addskill`);
}
function getAddskillOne(id: number) {
  return http.get(`/addskill/${id}`);
}

function getAddskillOneAllskill(id: number) {
  return http.get(`/addskill/${id}`);
}

function getAddskillOneAlllevel(id: number) {
  return http.get(`/addskill/${id}`);
}

function saveAddskill(addskill: Addskill[]) {
  return http.post(`/addskill`, addskill);
}

function updateAddskill(id: number, addskill: Addskill) {
  return http.patch(`/addskill/${id}`, addskill);
}

function deleteAddskill(id: number) {
  return http.delete(`/addskill/${id}`);
}

export default { getAddskills, saveAddskill, updateAddskill, deleteAddskill , getAddskillOne,getAddskillOneAllskill,getAddskillOneAlllevel};
