import type Certificates from "@/types/Certificates";
import http from "./axios";
function getCertificates() {
  return http.get(`/certificates`);
}
function getCertificatesOne(id: number) {
  return http.get(`/certificates/${id}`);
}

function saveCertificates(certificates: Certificates) {
  return http.post(`/certificates`, certificates);
}

function updateCertificates(id: number, certificates: Certificates) {
  return http.patch(`/certificates/${id}`, certificates);
}

function deleteCertificates(id: number) {
  return http.delete(`/certificates/${id}`);
}


export default { getCertificates, saveCertificates, updateCertificates, deleteCertificates ,getCertificatesOne};
