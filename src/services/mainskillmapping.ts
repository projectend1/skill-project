import type Mainskillmapping from "@/types/Mainskillmapping";
import http from "./axios";

function getMainskillmappings() {
  return http.get( `/mainskillmapping`);  
}

function getMainskillmappingsBySubjectId(id: number) {
  return http.get( `/mainskillmapping/getbysubjectId/${id}`);  
}

function getMainskillmappingOne(id: number) {
  return http.get(`/mainskillmapping/${id}`);
}

function saveMainskillmapping(mainskillmapping:Mainskillmapping) {
  return http.post(`/mainskillmapping`, mainskillmapping);
}

function updateMainskillmapping(id: number, mainskillmapping: Mainskillmapping) {
  return http.patch(`/mainskillmapping/${id}`, mainskillmapping);
}

function deleteMainskillmapping(id: number) {
  return http.delete(`/mainskillmapping/${id}`);
}

export default { getMainskillmappings, saveMainskillmapping, updateMainskillmapping, deleteMainskillmapping,getMainskillmappingOne,getMainskillmappingsBySubjectId};
