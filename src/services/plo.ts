import type PLO from "@/types/PLO";
import http from "./axios";

function getPLOs() {
  return http.get( `/plo`);
}
function getPLOOne(id: number) {
  return http.get(`/plo/${id}`);
}

function savePLO(plo: PLO) {
  return http.post(`/plo`, plo);
}

function updatePLO(id: number, plo: PLO) {
  return http.patch(`/pol/${id}`, plo);
}

function deletePLO(id: number) {
  return http.delete(`/plo/${id}`);
}

export default { getPLOs, savePLO, updatePLO, deletePLO,getPLOOne };
