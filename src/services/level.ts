import type Level from "@/types/Level";
import http from "./axios";

function getLevels() {
  return http.get( `/level`);  
}
function getLevelOne(id: number) {
  return http.get(`/level/${id}`);
}

function saveLevel(level:Level) {
  return http.post(`/level`, level);
}

function updateLevel(id: number, level: Level) {
  return http.patch(`/level/${id}`, level);
}

function deleteLevel(id: number) {
  return http.delete(`/level/${id}`);
}

export default { getLevels, saveLevel, updateLevel, deleteLevel,getLevelOne };
