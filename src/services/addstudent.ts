import type Addstudent from '@/types/Addstudent'
import http from './axios'

function getAddstudents() {
  return http.get(`/addstudent`)
}
function getAddstudentByYear(id: string) {
  return http.get(`/addstudent/findAllByYear/${id}`)
}
function getAddstudentOne(id: number) {
  return http.get(`/addstudent/${id}`)
}

function saveAddstudent(addstudent: Addstudent) {
  return http.post(`/addstudent`, addstudent)
}

function updateAddstudent(id: number, addstudent: Addstudent) {
  return http.patch(`/addstudent/${id}`, addstudent)
}

function deleteAddstudent(id: number) {
  return http.delete(`/addstudent/${id}`)
}

export default {
  getAddstudents,
  saveAddstudent,
  updateAddstudent,
  deleteAddstudent,
  getAddstudentOne,
  getAddstudentByYear
}
