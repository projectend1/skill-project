import type Result from "@/types/Result";
import http from "./axios";

function getResults() {
  return http.get( `/result`);  
}
function getResultByStudent(id:number) {
  return http.get( `/result/getfindAllStudent/${id}`);  
}

function getResultOne(id: number) {
  return http.get(`/result/${id}`);
}

function saveResult(result:Result) {
  return http.post(`/result`, result);
}

function updateResult(id: number, result: Result) {
  return http.patch(`/result/${id}`, result);
}

function deleteResult(id: number) {
  return http.delete(`/result/${id}`);
}

export default { getResults, saveResult, updateResult, deleteResult,getResultOne,getResultByStudent};
