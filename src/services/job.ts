import type Job from "@/types/Job";
import http from "./axios";
function getJobs() {
  return http.get(`/job`);
}
function getJobOne(id: number) {
  return http.get(`/job/${id}`);
}

function saveJob(job: Job) {
  return http.post(`/job`, job);
}

function updateJob(id: number, job: Job) {
  return http.patch(`/job/${id}`, job);
}

function deleteJob(id: number) {
  return http.delete(`/job/${id}`);
}


export default { getJobs, saveJob, updateJob, deleteJob ,getJobOne};
