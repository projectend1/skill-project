import type Profile from "@/types/Profile";
import http from "./axios";
function getProfiles() {
  return http.get(`/profile`);
}
function getProfileOne(id: number) {
  return http.get(`/profile/${id}`);
}

function saveProfile(profile: Profile) {
  return http.post(`/profile`, profile);
}

function updateProfile(id: number, profile: Profile) {
  return http.patch(`/profile/${id}`, profile);
}

function deleteProfile(id: number) {
  return http.delete(`/profile/${id}`);
}


export default { getProfiles, saveProfile, updateProfile, deleteProfile ,getProfileOne};
