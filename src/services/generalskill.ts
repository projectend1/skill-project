import type Generalskill from "@/types/Generalskill";
import http from "./axios";

function getGeneralskills() {
  return http.get( `/generalskill`);  
}
function getGeneralskillOne(id: number) {
  return http.get(`/generalskill/${id}`);
}

function saveGeneralskill(generalskill: Generalskill) {
  return http.post(`/generalskill`, generalskill);
}

function updateGeneralskill(id: number, generalskill: Generalskill) {
  return http.patch(`/generalskill/${id}`, generalskill);
}

function deleteGeneralskill(id: number) {
  return http.delete(`/generalskill/${id}`);
}

export default { getGeneralskills, saveGeneralskill, updateGeneralskill, deleteGeneralskill,getGeneralskillOne };
