import router from '@/router'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})


// ทำการส่งคำขอ HTTP GET ไปยังเซิร์ฟเวอร์โดยใช้ Axios

instance.interceptors.request.use(
  async function (config) {
    // await delay(1000);
    const token = localStorage.getItem('token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }

    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  async function (res) {
    return res
  },
  function (error) {
    // Do something with request error
    // if (401 === error.response.status) {
    //   router.replace("/login");
    // }
    return Promise.reject(error)
  }
)


// ทำการส่งคำขอ HTTP GET ไปยังเซิร์ฟเวอร์โดยใช้ Axios
//  instance
//    .get('/subject')
//    .then((response) => {
//     const subjects = response.data.results;
//     // ทำอะไรกับข้อมูลที่ได้รับ เช่น นำมาเก็บไว้ในตัวแปรหรืออื่นๆ
//      console.log('ข้อมูลที่ได้รับ:', response.data)
//    })
//    .catch((error) => {
//      console.error('เกิดข้อผิดพลาด:', error)
//    })
export default instance
