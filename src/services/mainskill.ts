import type Mainskill from "@/types/Mainskill";
import http from "./axios";

function getMainskills() {
  return http.get( `/mainskill`);  
}
function getMainskillOne(id: number) {
  return http.get(`/mainskill/${id}`);
}

function saveMainskill(mainskill: Mainskill) {
  return http.post(`/mainskill`, mainskill);
}

function updateMainskill(id: number, mainskill: Mainskill) {
  return http.patch(`/mainskill/${id}`, mainskill);
}

function deleteMainskill(id: number) {
  return http.delete(`/mainskill/${id}`);
}

export default { getMainskills, saveMainskill, updateMainskill, deleteMainskill,getMainskillOne };
