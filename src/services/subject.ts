import type Subject from '@/types/Subject';
import http from './axios';

function getSubjects() {
  return http.get(`/subject`);
}

function getSubjectOne(id: number) {
  return http.get(`/subject/${id}`);
}

function saveSubject(subject: Subject) {
  return http.post(`/subject`, subject);
}

function updateSubject(id: number, subject: Subject) {
  return http.patch(`/subject/${id}`, subject);
}

function deleteSubject(id: number) {
  return http.delete(`/subject/${id}`);
}

export default { getSubjects, saveSubject, updateSubject, deleteSubject, getSubjectOne }
