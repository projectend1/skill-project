export default interface Information2 {
  id: number;
  idStudent: number;
  name: string;
  skill: string;
  level: string;
}
