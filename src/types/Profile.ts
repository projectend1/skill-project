export default interface ProfileData {
    nameEng: string;
    nameTh: string;
    birthdate:string;
    gender:string;
    nationality: string;
    userType:string;
    id:number;
    year:number;
    facuityEng:string;
    email:string;
    email1:string;
    address:string;
    phone:number;
    image: File | null;
  }