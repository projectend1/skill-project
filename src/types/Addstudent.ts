export default interface Addstudent {
  id?: number
  idstudent: string
  name: string
  lastname: string
  year: string
  mainLevel: string
  generalLevel: string
}
