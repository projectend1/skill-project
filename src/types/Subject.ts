export default interface Subject {
  id?: number;
  course: string;
  nameEng: string;
  nameTh: string;
  descriptionEng: string;
  descriptionTh: string;
}
