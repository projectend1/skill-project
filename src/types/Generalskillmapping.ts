import  type Generalskill from "./Generalskill";
import type PLO from "./PLO";
import type Subject from "./Subject";

export default interface Generalskillmapping {
     id?: number
    PLO_id: number
    course: string
    namegeneral: string
    expectLevel:number
    
  }
  // pl.PLO_id , sb.course , sb.nameEng , ms.namemainEng ,lv.level