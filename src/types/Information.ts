export default interface Information {
    id: number;
    course: number;
    nameEng: string;
    nameTh: string;
    descriptionEng: string;
    descriptionTh: string;
  }
  