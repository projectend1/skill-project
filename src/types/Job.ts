export default interface Job {
  id?: number;
  nameEng: string;
  nameTh: string;
  descriptionEng: string;
  descriptionTh: string;
 
  
}
