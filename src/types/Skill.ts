export default interface Skill {
  id?: number;
  nameEng: string;
  nameTh: string;
  descriptionEng: string;
  descriptionTh: string;
  mainSkill: string;

}
