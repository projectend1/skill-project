

export default interface Mainskillmapping {
  id?: number,
  PLO_id : number ,
  namemainEng:string,
  level:number,
  course: string,
   
}
// <template>
// <!-- your template code -->
// <tr v-for="item of mainskillmappingList" :key="item.id">
//   <td>{{ item.id }}</td>
//   <!-- แสดงข้อมูล PLO ที่มาจาก inner join -->
//   <td>{{ item.PLO_id }}</td>
//   <!-- แสดงข้อมูลชื่อสกิลที่มาจาก inner join -->
//   <td>{{ item.namemainEng }}</td>
//   <!-- แสดงข้อมูล Level ที่มาจาก inner join -->
//   <td>{{ item.level }}</td>
//   <!-- แสดงข้อมูลรหัสวิชาที่มาจาก inner join -->
//   <td>{{ item.course }}</td>
//   <!-- your other table cells -->
//   <!-- your popup dialog and delete confirmation dialog -->
// </tr>
// <!-- your other template code -->
// </template>
